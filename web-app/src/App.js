import './App.css';
import React from "react";

class App extends React.Component {
    static displayName = App.name;

    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true };
    }

    componentDidMount() {
        this.populateWeatherData();
    }

    static renderForecastsTable(forecasts) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel" border="1">
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Температура (C)</th>
                        <th>Температура (F)</th>
                        <th>Комментарий</th>
                    </tr>
                </thead>
                <tbody>
                    {forecasts.map(forecast =>
                        <tr key={forecast.date}>
                            <td style={tdStyle}>{forecast.date}</td>
                            <td style={tdStyle}>{forecast.temperatureC}</td>
                            <td style={tdStyle}>{forecast.temperatureF}</td>
                            <td style={tdStyle}>{forecast.summary}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : App.renderForecastsTable(this.state.forecasts);

        return (
            <div>
                <h1>Прогноз погоды</h1>                
                {contents}
            </div>
        );
    }

    async populateWeatherData() {
        const response = await fetch('https://localhost:5001/WeatherForecast');
        const data = await response.json();
        this.setState({ forecasts: data, loading: false });
    }
}

const tdStyle = {
    padding: "10px"
}

export default App;
